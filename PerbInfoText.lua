-----------------------------------------------------------------------------------------------
-- Client Lua Script for PerbInfoText
-- Copyright (c) NCsoft. All rights reserved
-----------------------------------------------------------------------------------------------
 
require "Window"
 
-----------------------------------------------------------------------------------------------
-- PerbInfoText Module Definition
-----------------------------------------------------------------------------------------------
local PerbInfoText = {} 
 
-----------------------------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------------------------
-- e.g. local kiExampleVariableMax = 999
 
-----------------------------------------------------------------------------------------------
-- Initialization
-----------------------------------------------------------------------------------------------
function PerbInfoText:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function PerbInfoText:Init()
    Apollo.RegisterAddon(self)
end
 

-----------------------------------------------------------------------------------------------
-- PerbInfoText OnLoad
-----------------------------------------------------------------------------------------------
function PerbInfoText:OnLoad()
    -- load our form file
	self.xmlDoc = XmlDoc.CreateFromFile("PerbInfoText.xml")
	self.xmlDoc:RegisterCallback("OnDocLoaded", self)
end

-----------------------------------------------------------------------------------------------
-- PerbInfoText OnDocLoaded
-----------------------------------------------------------------------------------------------
function PerbInfoText:OnDocLoaded()
	if self.xmlDoc == nil then return end
	
	Apollo.LoadSprites("BorderSprite.xml", "Perb")
	Apollo.RegisterEventHandler("WindowManagementReady", "OnWindowManagementReady", self)

	self.wndMain = Apollo.LoadForm(self.xmlDoc, "Background", "FixedHudStratum", self)

	self.timer = ApolloTimer.Create(1.0, true, "OnTimer", self)
	
	self.wndMain:Show(false, true)
end

function PerbInfoText:OnWindowManagementReady()
	Event_FireGenericEvent("WindowManagementAdd", {wnd = self.wndMain, strName = "PerbInfoText"})
end

function PerbInfoText:OnTimer()
	local unitPlayer = GameLib.GetPlayerUnit()
	if unitPlayer == nil then return end
	
	local stats = unitPlayer:GetBasicStats()
	local currentXP = GetXp()
	local neededXP = GetXpToNextLevel()
	local elderXP = GetPeriodicElderPoints()
	local xpPercent = stats.nLevel == 50 and math.floor(elderXP / 10500000 * 100) or math.floor(currentXP / neededXP * 100)
	local framesPerSecond = math.floor(GameLib.GetFrameRate())
	local latency = GameLib.GetLatency()
	local localTime = GameLib.GetLocalTime()
	local localHour = localTime.nHour > 12 and localTime.nHour - 12 or localTime.nHour == 0 and 12 or localTime.nHour
	local timeString = ""
	local ampm = localTime.nHour > 12 and " pm" or " am"
	
	if localHour < 10 then
		timeString = string.format("%01d:%02d", tostring(localHour), tostring(localTime.nMinute)) or ""
	else
		timeString = string.format("%02d:%02d", tostring(localHour), tostring(localTime.nMinute)) or ""
	end
	
	self.wndMain:FindChild("TextFrame"):SetText(xpPercent .. "% xp     " .. framesPerSecond .. " fps     " .. latency .. " ms     " .. timeString .. ampm)
	self.wndMain:Show(true, true)
end

local PerbInfoTextInst = PerbInfoText:new()
PerbInfoTextInst:Init()
