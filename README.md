# PerbInfoText #

* Displays xp, fps, latency, and local time.
* To move: Esc → Interface → Window → Enable movable.

![perbInfoText.png](https://bitbucket.org/repo/nbRLzB/images/3568886064-perbInfoText.png)

[Download](https://bitbucket.org/Perby/perbinfotext/downloads)